const redirectToPage = (link) => {
  window.location.href = link;
};

function showPage(shown, hidden) {
  document.getElementById(shown).style.display = "block";
  document.getElementById(hidden).style.display = "none";
}

function validateForm(){
  var nameContact = document.forms["contactForm"]["Nama_Lengkap"].value;
  var emailContact = document.forms["contactForm"]["No_KTP"].value;
  var alamatContact = document.forms["contactForm"]["No_Telepon"].value;
  var telpContact = document.forms["contactForm"]["Address"].value;

  if(nameContact==""){
    alert ("Name must be filled");
    return false;
  }
  if(emailContact==""){
    alert ("No KTP must be filled");
    return false;
  }
  if(alamatContact==""){
    alert ("No Telp must be filled");
    return false;
  }
  if(telpContact==""){
    alert ("Alamat must be filled");
    return false;
  }
}